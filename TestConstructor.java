
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.experimental.categories.Category;

public class TestConstructor {

	
	@Test
	@Category(ITestRight.class)
	public void testContructorRight() {
		String model="A";
		double pret=2000.000;
	    Masina masina =new Masina(model, pret);
		double expected_pret=2000.000;
		String expected_model="A";
        double delay=0.13;
	    assertEquals(expected_pret, masina.getPret(),delay);
	    assertEquals(expected_model, masina.getModel());
       System.out.println("test right for  constructor");

	}
	
	
	@Test
	@Category(ITestCrossCheck.class)
	public void testContructorCrossCheck() {
		String model="A";
		double pret=2000.000;
	    Masina masina =new Masina(model, pret);
	    Masina masin2=new Masina();
	    masin2.setModel(model);
	    masin2.setPret(pret);
        double delay=0.13;
	    assertEquals(masin2.getPret(), masina.getPret(),delay);
	    assertEquals(masin2.getModel(), masina.getModel());
	    	    System.out.println("test cross check for contructor");


	}
	
	
	
	
	
	
	

}
