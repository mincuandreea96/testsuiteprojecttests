

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.experimental.categories.Category;

public class TestSetModel {

	@Test
	@Category(ITestRight.class)
	public void testSetModelRight() {
		String actual_model="A";
	    Masina masina =new Masina();
	    masina.setModel(actual_model);
	    String expected_model_value="A";
	    
	    assertEquals(expected_model_value, actual_model);	
	    	    System.out.println("test right for setModel function");

	}
	
	@Test
	@Category(ITestCrossCheck.class)
	public void testSetModelCrossCheck() {
		String actual_model="A";
	    Masina masina =new Masina();
	    masina.setModel(actual_model);
	    
	    Masina masina2=new Masina(actual_model, 200);
	    
	    assertEquals(masina2.getModel(), masina.getModel());
	    	    System.out.println("test cross check for setModel function");
	
	}
	
	
	
	
	
	
	
	
}
